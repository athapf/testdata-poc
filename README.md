
Creation of random test data.
Individual data fields are generated depending on the fields previously created.

Creating the test data under Linux (or Cygwin) with "xsltproc"

```bash
xsltproc xsl/gentdat.xsl xml/usedcar.xml
```

To produce more then 1 test data record use string parameter with name "count"

```bash
xsltproc --stringparam count 5 xsl/gentdat.xsl xml/usedcar.xml
```

Formatting output with "xmllint"

```bash
xsltproc xsl/gentdat.xsl xml/usedcar.xml | xsllint --format -
```
