<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:math="http://exslt.org/math"
                xmlns:dyn="http://exslt.org/dynamic"
                xmlns:date="http://exslt.org/dates-and-times"
                extension-element-prefixes="math date dyn">

    <xsl:template name="id-with-check">
        <xsl:param name="depends"/>
        <xsl:param name="data"/>
        <xsl:param name="store"/>

        <xsl:variable name="id">
            <xsl:apply-templates select="./list|./sequence|./random">
                <xsl:with-param name="depends" select="$depends"/>
                <xsl:with-param name="data" select="$data"/>
                <xsl:with-param name="store" select="$store"/>
            </xsl:apply-templates>
        </xsl:variable>

        <xsl:variable name="name" select="../@name"/>
        <xsl:choose>
            <xsl:when test="$store//field[./@name=$name and ./@value=$id]">
                <xsl:call-template name="id-with-check">
                    <xsl:with-param name="depends" select="$depends"/>
                    <xsl:with-param name="data" select="$data"/>
                    <xsl:with-param name="store" select="$store"/>
                </xsl:call-template>
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="$id"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template match="id">
        <xsl:param name="depends"/>
        <xsl:param name="data"/>
        <xsl:param name="store"/>

        <xsl:call-template name="id-with-check">
            <xsl:with-param name="depends" select="$depends"/>
            <xsl:with-param name="data" select="$data"/>
            <xsl:with-param name="store" select="$store"/>
        </xsl:call-template>
    </xsl:template>

</xsl:stylesheet>
