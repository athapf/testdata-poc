<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:math="http://exslt.org/math"
                extension-element-prefixes="math">

    <xsl:template name="pick-element">
        <xsl:param name="liste"/>
        <xsl:param name="random"/>

        <xsl:if test="count($liste)&gt;0">
            <xsl:variable name="wert">
                <xsl:choose>
                    <xsl:when test="$liste[position()=1]/@weight">
                        <xsl:value-of select="$liste[position()=1]/@weight"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:text>1</xsl:text>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:variable>

            <xsl:choose>
                <xsl:when test="$random &lt;= $wert">
                    <xsl:value-of select="$liste[position()=1]/@value"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:call-template name="pick-element">
                        <xsl:with-param name="liste" select="$liste[position()&gt;1]"/>
                        <xsl:with-param name="random" select="$random - $wert"/>
                    </xsl:call-template>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:if>
    </xsl:template>

    <xsl:template name="count-weight">
        <xsl:param name="liste"/>

        <xsl:choose>
            <xsl:when test="count($liste)&lt;1">
                <xsl:text>0</xsl:text>
            </xsl:when>
            <xsl:when test="count($liste)=1">

                <xsl:choose>
                    <xsl:when test="$liste/@weight">
                        <xsl:value-of select="$liste/@weight"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:text>1</xsl:text>
                    </xsl:otherwise>
                </xsl:choose>

            </xsl:when>
            <xsl:otherwise>
                <xsl:variable name="count">
                    <xsl:call-template name="count-weight">
                        <xsl:with-param name="liste" select="$liste[position()&gt;1]"/>
                    </xsl:call-template>
                </xsl:variable>
                <xsl:variable name="element" select="$liste[position()=1]"/>
                <xsl:choose>
                    <xsl:when test="$element/@weight">
                        <xsl:value-of select="$element/@weight + $count"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="1 + $count"/>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template match="list">
        <xsl:param name="depends"/>

        <xsl:variable name="parent" select="./ancestor::field/@name"/>
        <xsl:variable name="elements"
                      select="$depends/depends[./@for=$parent]/element | ./element[not(./@value = $depends/depends[./@for=$parent]/exclude/@value)]"/>

        <xsl:variable name="count">
            <xsl:call-template name="count-weight">
                <xsl:with-param name="liste" select="$elements"/>
            </xsl:call-template>
        </xsl:variable>
        <xsl:variable name="random" select="(floor(math:random() * $count) mod $count) + 1"/>
        <xsl:call-template name="pick-element">
            <xsl:with-param name="liste" select="$elements"/>
            <xsl:with-param name="random" select="$random"/>
        </xsl:call-template>
    </xsl:template>

</xsl:stylesheet>
