<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
		xmlns:math="http://exslt.org/math"
                xmlns:dyn="http://exslt.org/dynamic"
                xmlns:date="http://exslt.org/dates-and-times"
                extension-element-prefixes="math date dyn">

  <xsl:import href="./exslt/date.format-date.function.xsl" />
  <xsl:import href="./exslt/date.add.function.xsl" />

  <xsl:template match="date">
    <xsl:param name="depends"/>
    <xsl:param name="data"/>
    <xsl:param name="store"/>

    <xsl:variable name="parent" select="../@name"/>
    <xsl:variable name="elements" select="$depends/depends[./@for=$parent]/node()[name(.)='from' or name(.)='to' or name(.)='expression'] | ./from[not($depends/depends[./@for=$parent]/from)] | ./to[not($depends/depends[./@for=$parent]/to)] | ./expression[not($depends/depends[./@for=$parent]/expression)]"/>
    
    <xsl:variable name="bereich" select="$elements[name(.)='to']/@value - $elements[name(.)='from']/@value + 1"/>
    <xsl:variable name="value" select="(floor(math:random() * $bereich) mod $bereich) + $elements[name(.)='from']/@value"/>
    <xsl:variable name="format">
      <xsl:choose>
	<xsl:when test="./@format">
	  <xsl:value-of select="./@format"/>
	</xsl:when>
	<xsl:otherwise>
	  <xsl:text>yyyy-MM-dd</xsl:text>
	</xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    
    <xsl:choose>
      <xsl:when test="$elements[name(.)='expression']">
        <xsl:value-of select="date:format-date(dyn:evaluate($elements[name(.)='expression']), $format)"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="date:format-date($value, $format)"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
  
</xsl:stylesheet>
