<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:math="http://exslt.org/math"
                extension-element-prefixes="math">
  
  <xsl:template name="multi-pick-element">
    <xsl:param name="liste"/>
    <xsl:param name="random"/>
    
    <xsl:if test="count($liste)&gt;0">
      <xsl:variable name="wert">
        <xsl:choose>
          <xsl:when test="$liste[position()=1]/@weight">
            <xsl:value-of select="$liste[position()=1]/@weight"/>
          </xsl:when>
          <xsl:otherwise>
            <xsl:text>1</xsl:text>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:variable>
      
      <xsl:choose>
        <xsl:when test="$random &lt;= $wert">
          <xsl:value-of select="$liste[position()=1]/@value"/>
        </xsl:when>
        <xsl:otherwise>
          <xsl:call-template name="multi-pick-element">
            <xsl:with-param name="liste" select="$liste[position()&gt;1]"/>
            <xsl:with-param name="random" select="$random - $wert"/>
          </xsl:call-template>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:if>
  </xsl:template>

  <xsl:template name="multi-count-weight">
    <xsl:param name="liste"/>
    
    <xsl:choose>
      <xsl:when test="count($liste)&lt;1">
        <xsl:text>0</xsl:text>
      </xsl:when>
      <xsl:when test="count($liste)=1">

        <xsl:choose>
          <xsl:when test="$liste/@weight">
            <xsl:value-of select="$liste/@weight"/>
          </xsl:when>
          <xsl:otherwise>
            <xsl:text>1</xsl:text>
          </xsl:otherwise>
        </xsl:choose>

      </xsl:when>
      <xsl:otherwise>
        <xsl:variable name="count">
          <xsl:call-template name="multi-count-weight">
            <xsl:with-param name="liste" select="$liste[position()&gt;1]"/>
          </xsl:call-template>
        </xsl:variable>
        <xsl:variable name="element" select="$liste[position()=1]"/>
        <xsl:choose>
          <xsl:when test="$element/@weight">
            <xsl:value-of select="$element/@weight + $count"/>
          </xsl:when>
          <xsl:otherwise>
            <xsl:value-of select="1 + $count"/>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <xsl:template name="multi-recursion">
    <xsl:param name="count"/>
    <xsl:param name="list"/>

    <xsl:variable name="weight">
      <xsl:call-template name="multi-count-weight">
        <xsl:with-param name="liste" select="$list"/>
      </xsl:call-template>
    </xsl:variable>
    <xsl:variable name="random" select="(floor(math:random() * $weight) mod $weight) + 1"/>
    <xsl:variable name="selected">
      <xsl:call-template name="multi-pick-element">
        <xsl:with-param name="liste" select="$list"/>
        <xsl:with-param name="random" select="$random"/>
      </xsl:call-template>
    </xsl:variable>

    <xsl:if test="$count&gt;1">
      <xsl:call-template name="multi-recursion">
        <xsl:with-param name="count" select="$count - 1"/>
        <xsl:with-param name="list" select="$list[not(./@value=$selected)]"/>
      </xsl:call-template>
    </xsl:if>

    <xsl:element name="element">
      <xsl:attribute name="value">
        <xsl:value-of select="$selected"/>
      </xsl:attribute>
    </xsl:element>
  </xsl:template>

  <xsl:template match="multi">
    <xsl:param name="depends"/>
    <xsl:param name="data"/>

    <xsl:variable name="parent" select="../@name"/>
    <xsl:variable name="elements" select="$depends/depends[./@for=$parent]/node()[name(.)='from' or name(.)='to' or name(.)='element'] | ./from[not($depends/depends[./@for=$parent]/from)] | ./to[not($depends/depends[./@for=$parent]/to)] | ./expression[not($depends/depends[./@for=$parent]/expression)] | ./element"/>

    <xsl:variable name="bereich" select="$elements[name(.)='to']/@value - $elements[name(.)='from']/@value + 1"/>
    <xsl:variable name="value" select="(floor(math:random() * $bereich) mod $bereich) + $elements[name(.)='from']/@value"/>
 
    <xsl:call-template name="multi-recursion">
      <xsl:with-param name="count" select="$value"/>
      <xsl:with-param name="list" select="$elements[name(.)='element']"/>
    </xsl:call-template>
  </xsl:template>

</xsl:stylesheet>
