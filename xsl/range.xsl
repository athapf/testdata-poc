<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:math="http://exslt.org/math"
                xmlns:dyn="http://exslt.org/dynamic"
                xmlns:date="http://exslt.org/dates-and-times"
                extension-element-prefixes="math date dyn">

    <xsl:template match="range">
        <xsl:param name="depends"/>
        <xsl:param name="data"/>

        <xsl:variable name="parent" select="../@name"/>
        <xsl:variable name="elements"
                      select="$depends/depends[./@for=$parent]/node()[name(.)='from' or name(.)='to' or name(.)='expression'] | ./from[not($depends/depends[./@for=$parent]/from)] | ./to[not($depends/depends[./@for=$parent]/to)] | ./expression[not($depends/depends[./@for=$parent]/expression)]"/>

        <xsl:variable name="bereich" select="$elements[name(.)='to']/@value - $elements[name(.)='from']/@value + 1"/>
        <xsl:variable name="value"
                      select="(floor(math:random() * $bereich) mod $bereich) + $elements[name(.)='from']/@value"/>

        <xsl:choose>
            <xsl:when test="$elements[name(.)='expression']">
                <xsl:value-of select="dyn:evaluate($elements[name(.)='expression'])"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="$value"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

</xsl:stylesheet>
