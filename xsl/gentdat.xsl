<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:math="http://exslt.org/math"
                xmlns:exsl="http://exslt.org/common"
                extension-element-prefixes="math exsl">

    <xsl:output method="xml" encoding="utf-8"/>

    <xsl:include href="./list.xsl"/>
    <xsl:include href="./text.xsl"/>
    <xsl:include href="./id.xsl"/>
    <xsl:include href="./multi.xsl"/>
    <xsl:include href="./range.xsl"/>
    <xsl:include href="./date.xsl"/>

    <xsl:param name="count">1</xsl:param>

    <xsl:variable name="doc" select="//field"/>

    <xsl:template match="field">
        <xsl:param name="depends"/>
        <xsl:param name="data"/>
        <xsl:param name="store"/>

        <xsl:element name="field">
            <xsl:attribute name="name">
                <xsl:value-of select="./@name"/>
            </xsl:attribute>
            <xsl:attribute name="type">
                <xsl:choose>
                    <xsl:when test="./@type">
                        <xsl:value-of select="./@type"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:text>string</xsl:text>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:attribute>
            <xsl:if test="count(./list|./range|./id|./text|./date)&gt;0">
                <xsl:attribute name="value">
                    <xsl:apply-templates select="./list|./range|./id|./text|./date">
                        <xsl:with-param name="depends" select="$depends"/>
                        <xsl:with-param name="data" select="$data"/>
                        <xsl:with-param name="store" select="$store"/>
                    </xsl:apply-templates>
                </xsl:attribute>
            </xsl:if>
            <xsl:apply-templates select="./multi">
                <xsl:with-param name="depends" select="$depends"/>
                <xsl:with-param name="data" select="$data"/>
                <xsl:with-param name="store" select="$store"/>
            </xsl:apply-templates>
        </xsl:element>
    </xsl:template>

    <xsl:template name="field-depends">
        <xsl:param name="field"/>

        <xsl:variable name="name" select="$field/@name"/>
        <xsl:variable name="value" select="$field/@value"/>

        <xsl:copy-of select="$doc//depends[../@value=$value and (../../@for=$name or ../../../@name=$name)]"/>
    </xsl:template>

    <xsl:template name="field-work">
        <xsl:param name="list"/>
        <xsl:param name="depends"/>
        <xsl:param name="data"/>
        <xsl:param name="store"/>

        <xsl:choose>
            <xsl:when test="count($list) &gt; 0">
                <xsl:variable name="element">
                    <xsl:apply-templates select="$list[position()=1]">
                        <xsl:with-param name="depends" select="$depends"/>
                        <xsl:with-param name="data" select="$data"/>
                        <xsl:with-param name="store" select="$store"/>
                    </xsl:apply-templates>
                </xsl:variable>

                <xsl:variable name="dep">
                    <xsl:call-template name="field-depends">
                        <xsl:with-param name="field" select="exsl:node-set($element)/*"/>
                    </xsl:call-template>
                </xsl:variable>

                <xsl:copy-of select="exsl:node-set($element)/*"/>

                <xsl:variable name="newdepends">
                    <xsl:copy-of select="$depends/*"/>
                    <xsl:copy-of select="exsl:node-set($dep)/*"/>
                </xsl:variable>

                <xsl:variable name="newdata">
                    <xsl:copy-of select="$data/*"/>
                    <xsl:copy-of select="exsl:node-set($element)/*"/>
                </xsl:variable>

                <xsl:choose>
                    <xsl:when test="count($list) &gt; 1">
                        <xsl:call-template name="field-work">
                            <xsl:with-param name="list" select="$list[position() &gt; 1]"/>
                            <xsl:with-param name="depends" select="exsl:node-set($newdepends)"/>
                            <xsl:with-param name="data" select="exsl:node-set($newdata)"/>
                            <xsl:with-param name="store" select="$store"/>
                        </xsl:call-template>
                    </xsl:when>
                </xsl:choose>
            </xsl:when>
        </xsl:choose>
    </xsl:template>

    <xsl:template name="data-count">
        <xsl:param name="cnt">1</xsl:param>
        <xsl:param name="store"/>

        <xsl:variable name="depends"/>
        <xsl:variable name="data"/>

        <xsl:if test="$cnt &gt; 0">
            <xsl:variable name="block">
                <xsl:element name="data">
                    <xsl:attribute name="number">
                        <xsl:value-of select="count($store/data)+1"/>
                    </xsl:attribute>
                    <xsl:call-template name="field-work">
                        <xsl:with-param name="list" select="/data/field"/>
                        <xsl:with-param name="depends" select="exsl:node-set($depends)"/>
                        <xsl:with-param name="data" select="exsl:node-set($data)"/>
                        <xsl:with-param name="store" select="$store"/>
                    </xsl:call-template>
                </xsl:element>
            </xsl:variable>

            <xsl:copy-of select="exsl:node-set($block)/*"/>

            <xsl:variable name="newstore">
                <xsl:copy-of select="$store/*"/>
                <xsl:copy-of select="exsl:node-set($block)/*"/>
            </xsl:variable>

            <xsl:if test="$cnt &gt; 1">
                <xsl:call-template name="data-count">
                    <xsl:with-param name="cnt" select="$cnt - 1"/>
                    <xsl:with-param name="store" select="exsl:node-set($newstore)"/>
                </xsl:call-template>
            </xsl:if>
        </xsl:if>
    </xsl:template>

    <xsl:template match="/">
        <xsl:variable name="store"/>

        <xsl:element name="set">
            <xsl:call-template name="data-count">
                <xsl:with-param name="cnt" select="$count"/>
                <xsl:with-param name="store" select="exsl:node-set($store)"/>
            </xsl:call-template>
        </xsl:element>
    </xsl:template>

</xsl:stylesheet>
