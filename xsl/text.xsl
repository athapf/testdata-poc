<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:math="http://exslt.org/math"
                xmlns:dyn="http://exslt.org/dynamic"
                xmlns:date="http://exslt.org/dates-and-times"
                extension-element-prefixes="math date dyn">

    <xsl:variable name="lowerCase">abcdefghijklmnopqrstuvwxyz</xsl:variable>
    <xsl:variable name="upperCase">ABCDEFGHIJKLMNOPQRSTUVWXYZ</xsl:variable>
    <xsl:variable name="numeric">0123456789</xsl:variable>
    <xsl:variable name="hex">0123456789ABCDEF</xsl:variable>

    <xsl:template name="filler">
        <xsl:param name="count"/>
        <xsl:if test="$count&gt;0">
            <xsl:text>0</xsl:text>
            <xsl:if test="$count&gt;0">
                <xsl:call-template name="filler">
                    <xsl:with-param name="count" select="$count - 1"/>
                </xsl:call-template>
            </xsl:if>
        </xsl:if>
    </xsl:template>

    <xsl:template match="sequence">
        <xsl:param name="data"/>
        <xsl:param name="store"/>

        <xsl:variable name="name" select="../../@name"/>
        <xsl:variable name="start">
            <xsl:choose>
                <xsl:when test="./@start">
                    <xsl:value-of select="./@start"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:text>1</xsl:text>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        <xsl:variable name="step">
            <xsl:choose>
                <xsl:when test="./@step">
                    <xsl:value-of select="./@step"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:text>1</xsl:text>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        <xsl:variable name="count" select="count($store/data/field[./@name=$name])"/>

        <xsl:variable name="value" select="($count * $step ) + $start"/>

        <xsl:value-of select="./@prefix"/>
        <xsl:if test="./@digits">
            <xsl:call-template name="filler">
                <xsl:with-param name="count" select="./@digits - string-length($value)"/>
            </xsl:call-template>
        </xsl:if>
        <xsl:value-of select="$value"/>
        <xsl:value-of select="./@suffix"/>
    </xsl:template>

    <xsl:template name="random-write">
        <xsl:param name="count"/>
        <xsl:param name="string"/>

        <xsl:if test="$count&gt;0">
            <xsl:variable name="value"
                          select="(floor(math:random() * string-length($string)) mod string-length($string)) + 1"/>
            <xsl:value-of select="substring($string,$value,1)"/>

            <xsl:if test="$count&gt;1">
                <xsl:call-template name="random-write">
                    <xsl:with-param name="count" select="$count - 1"/>
                    <xsl:with-param name="string" select="$string"/>
                </xsl:call-template>
            </xsl:if>
        </xsl:if>
    </xsl:template>

    <xsl:template match="random">
        <xsl:variable name="size">
            <xsl:choose>
                <xsl:when test="./@size">
                    <xsl:value-of select="./@size"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:text>5</xsl:text>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        <xsl:variable name="string">
            <xsl:choose>
                <xsl:when test="./@type='hex'">
                    <xsl:value-of select="$hex"/>
                </xsl:when>
                <xsl:when test="./@type='numeric'">
                    <xsl:value-of select="$numeric"/>
                </xsl:when>
                <xsl:when test="./@type='lower'">
                    <xsl:value-of select="$lowerCase"/>
                </xsl:when>
                <xsl:when test="./@type='upper'">
                    <xsl:value-of select="$upperCase"/>
                </xsl:when>
                <xsl:when test="./@type='alpha'">
                    <xsl:value-of select="concat($upperCase,$lowerCase)"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="concat($upperCase,$numeric,$lowerCase)"/>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        <xsl:value-of select="./@prefix"/>
        <xsl:call-template name="random-write">
            <xsl:with-param name="count" select="$size"/>
            <xsl:with-param name="string" select="$string"/>
        </xsl:call-template>
        <xsl:value-of select="./@suffix"/>
    </xsl:template>

    <xsl:template match="text">
        <xsl:param name="depends"/>
        <xsl:param name="data"/>
        <xsl:param name="store"/>

        <xsl:apply-templates select="./list|./sequence|./random">
            <xsl:with-param name="depends" select="$depends"/>
            <xsl:with-param name="data" select="$data"/>
            <xsl:with-param name="store" select="$store"/>
        </xsl:apply-templates>
    </xsl:template>

</xsl:stylesheet>
